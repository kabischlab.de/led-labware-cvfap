# Description
These are STL and CAD files created at the kabisch-lab for blue-light LED control of the *Chlorella varabilis* photodecarboxylase in the oleaginous yeast *Yarrowia lipolytica* for the production of biofuels as well as on spore surfaces.
# Citation
## MTP holder for shaker
If you use this labware for your own experiments please cite the following publication:
> [Drop-in biofuel production using fatty acid photodecarboxylase from *Chlorella variabilis* in the oleaginous yeast *Yarrowia lipolytica*, Biotechnology for Biofuels (2019)](https://biotechnologyforbiofuels.biomedcentral.com/articles/10.1186/s13068-019-1542-4)<

## MTP 
If you use the LED-Matrix please cite the above publication and additionaly cite the original work this device is based upon:
> Hennemann J, Iwasaki RS, Grund TN, Diensthuber RP, Richter F, Möglich A. Optogenetic control by pulsed illumination. ChemBioChem. 2018;19:1296–304.