//~ Rack for shacking MTP in Regalux boxes with LED array
//~https://www.bauhaus.info/kunststoffboxen/regalux-clear-box-xxs/p/20187327
//~https://www.bauhaus.info/kunststoffboxen/regalux-clear-box-deckel-xxs/p/20187336
//~ Version 01
//~ // Author and Copyright johannes@kabischlab.de

  //~ Dimensionen Platte
  Length_Plate=175;
  Width_Plate=107;
  Height_Plate=10;

  //~ Dimensionen MTP
  Length_MTP= 127.76+1;//~ !!!Plate is rotated
  Width_MTP=85.48+1;//~ !!!Plate is rotated
  Height_MTP=35;
  
  //~ Positionen MTP
  Pos_y_MTP=(Width_Plate-Length_MTP)/2;

difference(){
//~ Baseplate with waterwells
  union(){color([0.01,0.36,0.58])
    cube([Length_Plate,Width_Plate,Height_Plate+4]);
        //~ Ecke hl
        union(){
        difference(){
            translate([5,Width_Plate-5,0])color([0.01,0.36,0.58])
              cylinder(Height_Plate+Height_MTP,25,25);
            translate([0,Width_Plate-5,3])
             cylinder(Height_Plate+Height_MTP+2,16,16);  
        }
//        translate([Length_Plate+9,Width_Plate+14.5,Height_Plate+Height_MTP])
//cylinder(5,2,5);
    }
        //~ Ecke hr
          union(){
        difference(){
            translate([Length_Plate,Width_Plate-5,0])color([0.01,0.36,0.58])
                cylinder(Height_Plate+Height_MTP,25,25);
              translate([Length_Plate,Width_Plate-5,3])
                cylinder(Height_Plate+Height_MTP+2,16,16);  
          }
//translate([Length_Plate+9,-14.5,Height_Plate+Height_MTP])
//cylinder(5,2,5);
    }
        //~ Ecke vl
          union(){
        difference(){
            translate([5,5,0])color([0.01,0.36,0.58])
              cylinder(Height_Plate+Height_MTP,25,25);
            translate([0,5,3])
              cylinder(Height_Plate+Height_MTP+2,16,16);
        }
//translate([-6, Width_Plate+14.5,Height_Plate+Height_MTP])
//cylinder(5,2,5);
    }
        //~ Ecke vr
        union(){
        difference(){
            translate([Length_Plate,5,0])color([0.01,0.36,0.58])
              cylinder(Height_Plate+Height_MTP,25,25);
            translate([Length_Plate,5,3]) 
            cylinder(Height_Plate+Height_MTP+2,16,16);
        }
//translate([-6,-14.5,Height_Plate+Height_MTP])
//cylinder(5,2,5);
    }
        //~ MTP holder
        //~ translate([10,1.5,0])      
    //~ cube([Length_Plate-20,Width_Plate-3,Height_Plate+5]);    
    
        //~hooks for X-rubberbands
       }
//~ MTP-Turm
  //~ MTP-Holder left
    translate([Width_MTP-1,Pos_y_MTP,Height_Plate-2])
    rotate([0,0,90])
      cube([Length_MTP,Width_MTP+2.6,Height_MTP+3]);
  //~ MTP-Holder right
    translate([2*Width_MTP+5,Pos_y_MTP,Height_Plate-2])
    rotate([0,0,90])
      cube([Length_MTP,Width_MTP+2.6,Height_MTP+6]);
}
