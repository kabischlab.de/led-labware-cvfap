//LED cover for roller drums
// copyright Johannes Kabisch
//kabisch-lab.de
//v 0.1

//Bucket
radius_bucket=170.0/2;
height_bucket=100.0;
thickness_bucket=5;

//rod fixation
radius_rod=30/2;
height_rod=15.0;
radius_hole=12.5/2;

//fan
fan_opening_radius=40/2;
screw_radius=3.5/2;
screw_distance=(40-31.75)/2;

$fn = 40;

//bucket
difference(){
union(){
    translate([0,0,-45])    
cylinder(height_rod,radius_rod+5,radius_rod+2, center=true);
difference(){
cylinder(height_bucket+thickness_bucket,radius_bucket+thickness_bucket,radius_bucket+thickness_bucket,center=true);
translate([0,0,-47])    
cylinder(height_bucket,radius_bucket,radius_bucket);
}
}
//rod opening
translate([0,0,-70])    
cylinder(height_rod+80,radius_hole,radius_hole);
// fan 1
translate([-50,0,-70])    
cylinder(height_rod+80,fan_opening_radius,fan_opening_radius);
// screws fan 1
translate([-50+fan_opening_radius-screw_distance,0+fan_opening_radius,-70])    
cylinder(height_rod+80,screw_radius,screw_radius);

translate([-50-fan_opening_radius+screw_distance,0+fan_opening_radius,-70])    
cylinder(height_rod+80,screw_radius,screw_radius);

translate([-50+fan_opening_radius-screw_distance,0-fan_opening_radius,-70])    
cylinder(height_rod+80,screw_radius,screw_radius);

translate([-50-fan_opening_radius+screw_distance,0-fan_opening_radius,-70])    
cylinder(height_rod+80,screw_radius,screw_radius);
// fan 2
translate([50,0,-70])    
cylinder(height_rod+80,fan_opening_radius,fan_opening_radius);
//screws fan 2
translate([50+fan_opening_radius-screw_distance,0+fan_opening_radius,-70])    
cylinder(height_rod+80,screw_radius,screw_radius);
translate([50-fan_opening_radius+screw_distance,0+fan_opening_radius,-70])    
cylinder(height_rod+80,screw_radius,screw_radius);
translate([50+fan_opening_radius-screw_distance,0-fan_opening_radius,-70])    
cylinder(height_rod+80,screw_radius,screw_radius);
translate([50-fan_opening_radius+screw_distance,0-fan_opening_radius,-70])    
cylinder(height_rod+80,screw_radius,screw_radius);
}
//}