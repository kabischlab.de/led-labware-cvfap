//led-strip holder multifors bioreactors from infors
//Version v0.2
// Author and Copyright johannes@kabischlab.de

//LED strip
LED_width=8.2;
LED_heigth=5;
LED_length=128;

//Clamp
height_clamp=35;
radius_clamp=38.9;

//holder-clip
union(){color([0.01,0.36,0.58])
difference(){    
difference(){
//outer clip ring
translate([80/3.14,30,95])
cylinder(height_clamp-4,radius_clamp+3,radius_clamp+3);
//inner difference cylinder
translate([80/3.14,30,94])
cylinder(height_clamp,radius_clamp,radius_clamp);
}
translate([-20,50,0]) 
cube([90,45,130]);
}
difference(){color([0.01,0.36,0.58])
translate([3.0,-9,0])    
cube([46,10,126]);
//strip-indents
//1
translate([3.9,-2.5,-1])
cube([LED_width,LED_heigth,LED_length]);
cylinder([LED_width,LED_heigth,LED_length]);
//2
translate([12.8,-2.5,-1])
cube([LED_width,LED_heigth,LED_length]);
//3
translate([21.8,-2.5,-1])
cube([LED_width,LED_heigth,LED_length]);
//4
translate([30.6,-2.5,-1])
cube([LED_width,LED_heigth,LED_length]);
//5
translate([39.6,-2.5,-1])
cube([LED_width,LED_heigth,LED_length]);
}
translate([3,1,0])color([0.01,0.36,0.58])
cube([LED_width/5,LED_heigth/10,LED_length-2]);
translate([11.9,1,0])color([0.01,0.36,0.58])
cube([LED_width/5,LED_heigth/10,LED_length-2]);
translate([20.9,1,0])color([0.01,0.36,0.58])
cube([LED_width/5,LED_heigth/10,LED_length-2]);
translate([29.7,1,0])color([0.01,0.36,0.58])
cube([LED_width/5,LED_heigth/10,LED_length-2]);
translate([38.7,1,0])color([0.01,0.36,0.58])
cube([LED_width/5,LED_heigth/10,LED_length-2]);
translate([47.4,1,0])color([0.01,0.36,0.58])
cube([LED_width/5,LED_heigth/10,LED_length-2]);
}